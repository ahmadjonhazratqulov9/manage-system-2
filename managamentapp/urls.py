from django.urls import path
from .views import *

urlpatterns = [
    path('show',show, name='show'),
    path('',ShowLoginPage, name='login_page'),
    path('get_user_details',GetUserDetails, name='get_user'),
    path('log_out',LogOut, name='log_out'),
    path('doLogin',doLogin, name='do_login'),
]