import datetime
import requests
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from managamentapp.EmailBackEnd import EmailBackEnd
from managamentapp.models import CustomUser

def show(request):
    return render(request, 'demo.html')


def ShowLoginPage(request):
    return render(request, "login_page.html")


def doLogin(request):
    if request.method != "POST":
        return HttpResponse("<h1> Method not allowed  POST qilib junatilsa shuunday habar keladi</h1>")
    else:
        user = EmailBackEnd.authenticate(request, username=request.POST.get("email"), password=request.POST.get("password"))

        if user != None:
            login(request, user)

            return HttpResponse("Email: "+request.POST.get("email")+"Password: "+request.POST.get("password"))
        else:
            messages.error(requests,"Invalid login details")
            return HttpResponseRedirect("/")


def GetUserDetails(request):
    if request.user != None:
        return HttpResponse("User: "+request.user.email+"usertype: "+str(request.user.user_type))
    else:
        return HttpResponse("Please Login First")


def LogOut(request):
    logout(request)
    return HttpResponseRedirect("/")